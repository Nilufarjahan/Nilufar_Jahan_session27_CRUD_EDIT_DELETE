<?php

require_once("../../../vendor/autoload.php");
use App\BookTitle\BookTitle;

$serial=0;
$bookObject = new BookTitle();
$allData=$bookObject->index("obj");
?>
<!DOCTYPE html>
<html lang="en" xmlns:border="http://www.w3.org/1999/xhtml">
<head>
  <title>Atomic Project Book</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container"style="height:400px;background-color: cadetblue";>
  <h2>Information Of Book And Author</h2>
   <table class="table table-bordered">
    <thead>
      <tr>
          <th style="text-align: center">Serial</th>
        <th style="text-align: center">Id</th>
        <th style="text-align: center">Book Title</th>
        <th style="text-align: center">Author Name</th>
          <th style="text-align: center">Action </th>


      </tr>
    </thead>
    <tbody>
<tr>

<?php
      foreach($allData as $oneData)
      {
          ?>

<tr>
    <td><?php echo ++$serial ?> </td>
            <td><?php echo $oneData->id ?> </td>
             <td><?php echo $oneData->book_title ?></td>
             <td><?php echo $oneData->author_name ?></td>
            <?php  echo "<td> <a href=\"edits.php?id=$oneData->id\"class=\"btn btn-warning\" role=\"button\">Edit</a>  ";?>
           <?php  echo " <a href=\"delete.php?id=$oneData->id\"class=\"btn btn-danger\" role=\"button\">Delete</a> </td> ";?>
</tr>
    <?php  }
    ?>
</tr>

    </tbody>
  </table>
</div>

</body>
</html>

