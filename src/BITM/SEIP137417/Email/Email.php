<?php
namespace App\Email;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;
class Email extends DB
{
    public $id="";
    public $name="";
    public $email="";

    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }

        if (array_key_exists('email',$data))
        {
            $this->email=$data['email'];
        }


    }
    public function store(){

        $arrData  = array($this->name,$this->email);

        $sql = "INSERT INTO email( name, email) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Email: $this->email ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Email: $this->email ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }
    public function index(){
        echo "I am inside the index method of Email class";
    }
}