<?php
namespace App\Hobbies;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;

class Hobbies extends DB
{
    public $id="";
    public $name="";
    public $hobbies="";

    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
		if(array_key_exists('hobbies',$data))
        {
            
            $this->hobbies= implode(",",$data['hobbies']);
            
            
        }
       


    }
    public function store(){

        $arrData  = array($this->name,$this->hobbies);

        $sql = "INSERT INTO hobbies( name,hobbies) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Hobbies: $this->hobbies ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }
    public function index(){
        echo "I am inside the index method of Hobbies class";
    }
}