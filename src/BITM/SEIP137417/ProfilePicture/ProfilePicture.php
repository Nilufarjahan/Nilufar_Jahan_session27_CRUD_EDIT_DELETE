<?php
namespace App\ProfilePicture;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;


class ProfilePicture extends DB
{
    public $id="";
    public $name="";
    public $profile_picture="";

    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }

        if (array_key_exists('image_name',$data))
        {
            $this->profile_picture=$data['image_name'];
        }


    }
    public function store(){

       // $arrData  = array($this->name,$this->profile_picture);

        $sql = "INSERT INTO profile_picture(`name`,`profile_picture`) VALUES ( ?, ?)";

        $STH = $this->DBH->prepare($sql);
            $STH->bindParam(1,$this->name);
             $STH->bindParam(2,$this->profile_picture);

        $result=$STH->execute();
       // var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Profile Picture: $this->profile_picture] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] , [ Profile Picture: $this->profile_picture ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }
    public function index(){
        echo "I am inside the index method of profile class";
    }
}