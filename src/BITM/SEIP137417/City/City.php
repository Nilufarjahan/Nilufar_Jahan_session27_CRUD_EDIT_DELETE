<?php
namespace App\City;
use App\Model\database as DB;
use App\Message\Message;
use App\Utility\Utility;

class City extends DB
{
    public $id="";
    public $name="";
    public $city="";
    public $country="";
    public function __construct(){

        parent::__construct();
    }
    public function setData($data=NULL){
        if (array_key_exists('id',$data))
        {
            $this->id=$data['id'];

        }

        if (array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if (array_key_exists('country',$data))
        {
            $this->country=$data['country'];
        }
        if (array_key_exists('city',$data))
        {
            $this->city=$data['city'];
        }


    }
    public function store(){

        $arrData  = array($this->name,$this->country,$this->city);

        $sql = "INSERT INTO city( name,country,city) VALUES ( ?,?,?)";

        $STH = $this->DBH->prepare($sql);


        $result=$STH->execute($arrData);
        var_dump($result);
        if($result)
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] ,[ Country: $this->country ], [ City: $this->city ] <br> Data Has Been Inserted Successfully!</h5></div>");
        else
            Message::message("<div id='msg'></div><h5 align='center'>[ Name: $this->name ] ,[ Country: $this->country ], [ City: $this->city ] <br> Data Has not Been Inserted Successfully!</h5></div>");
        Utility::redirect('create.php');

    }
    public function index(){
        echo "I am inside the index method of city class";
    }
}